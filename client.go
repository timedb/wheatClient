package wheatClient

import (
	"encoding/gob"
	"fmt"
	"gitee.com/timedb/wheatClient/api"
	"github.com/timedb/wheatDFS/app"
	"github.com/timedb/wheatDFS/etc"
	"github.com/timedb/wheatDFS/fileKeyTorch"
	"github.com/timedb/wheatDFS/serverTorch"
	"github.com/timedb/wheatDFS/storage"
	"github.com/timedb/wheatDFS/torch/clientTorch"
	"github.com/timedb/wheatDFS/torch/hashTorch"
	"io"
	"os"
	"path"
	"sync"
	"sync/atomic"
	"time"
)

var oneLock sync.Once //单实例
var sysClient *Client

//检查函数
func examine(addr *etc.Addr) bool {

	if addr == nil {
		return false
	}

	req := app.MakeTraHeartReq()
	resp := new(app.ResponseBase)

	err := req.Do(addr, resp)
	if err == nil && resp.Successful() {
		return true
	}

	return false

}

// Cache 缓存器
type Cache struct {
	CachePath    string
	TrackerHosts []*etc.Addr //所有tracker的地址
	LeaderHosts  *etc.Addr   //leader
	Count        int32
}

//缓存到本地
func (c *Client) encode() {
	if c.cache.CachePath == "" {
		return
	}

	f, err := os.Create(c.cache.CachePath)
	if err != nil {
		return
	}
	defer f.Close()

	enc := gob.NewEncoder(f)
	enc.Encode(c.cache)

}

//读取本地缓存
func (c *Client) decode() {

	f, err := os.Open(c.cache.CachePath)
	if err != nil {
		return
	}

	defer f.Close()

	dec := gob.NewDecoder(f)

	dec.Decode(c)
}

type Client struct {
	cache      *Cache       //负载地址
	lockHosts  sync.RWMutex //读写锁
	lockLeader sync.RWMutex // leader锁
}

// MakeWheatClient 创建配连接器
func MakeWheatClient(conf string) *Client {

	etc.LoadConf(conf)

	app.MakeRpcConnectPool() //创建连接池

	oneLock.Do(func() {

		ip, _ := serverTorch.GetIPv4s()
		port, _ := serverTorch.GetAvailablePort()

		host, _ := etc.MakeAddr(ip, port, etc.StateDefault)
		etc.SysLocalHost = host

		client := new(Client)
		cache := new(Cache)
		cache.TrackerHosts = make([]*etc.Addr, 0)
		cache.LeaderHosts = etc.SysConf.TrackerConf.CandidateHost //获取一个初始leader
		cache.CachePath = etc.SysConf.Client.CachePath
		client.cache = cache

		client.decode() //根据缓存解码

		sysClient = client

		sysClient.updateTracker() //初始化

		go sysClient.work() //维护工作

	})

	return sysClient
}

//获取leader
func (c *Client) getLeader() *etc.Addr {
	c.lockLeader.RLock()
	defer c.lockLeader.RUnlock()

	return c.cache.LeaderHosts
}

//修改leader地址
func (c *Client) putLeader(host *etc.Addr) {
	c.lockLeader.Lock()
	defer c.lockLeader.Unlock()

	c.cache.LeaderHosts = host
}

//获取一个服务的tracker
func (c *Client) getTrackerHost() *etc.Addr {
	atomic.AddInt32(&c.cache.Count, 1) //负载添加
	c.lockHosts.RLock()
	defer c.lockHosts.RUnlock()
	if len(c.cache.TrackerHosts) == 0 {
		return c.getLeader()
	}

	return c.cache.TrackerHosts[int(c.cache.Count)%len(c.cache.TrackerHosts)]

}

//维护Tracker
func (c *Client) updateTracker() {
	req := app.MakeTraReportTraHosts(nil, nil)
	resp := new(app.TraReportTraHostsResp)
	err := req.Do(c.getLeader(), resp)

	if err != nil {
		fmt.Println(err)
	}

	if !resp.Successful() {
		return
	}

	c.lockHosts.Lock()
	defer c.lockHosts.Unlock()

	//转发更新leader
	if resp.TransAddr != nil {
		c.putLeader(resp.TransAddr) //更新leader
	}

	c.cache.TrackerHosts = resp.TraHosts

}

//维护leader
func (c *Client) updateLeader() {
	if examine(c.getLeader()) { //leader稳定
		return
	}

	for _, host := range c.cache.TrackerHosts {
		if examine(host) {
			c.putLeader(host)
			return
		}
	}

}

func (c *Client) work() {
	cacheTime := time.NewTicker(time.Minute * 2)
	syncLeader := time.NewTicker(time.Second * 20)

	select {
	case <-cacheTime.C:
		//同步
		c.encode()
	case <-syncLeader.C:
		c.updateTracker()
	}

}

//TrackerApi

// GetStorageAddr 获取一个Storage负载
func (c *Client) GetStorageAddr() (*etc.Addr, error) {

	//随机获取一个tracker
	tra := c.getTrackerHost()

	return api.GetStoAddr(tra)
}

//GetEso 获取eso
func (c *Client) GetEso(token string) (*app.EsoData, error) {
	host := c.getTrackerHost()
	return api.GetExo(host, token)
}

// VerifyFileIsExistByHash 查询一个文件是否存在Eso
func (c *Client) VerifyFileIsExistByHash(hash string, ext string) bool {
	host := c.getTrackerHost()

	return api.VerifyFileIsExist(host, hash, ext)
}

//GetHashByFile 计算文件hash
func (c *Client) GetHashByFile(filePath string) (string, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer f.Close()

	return hashTorch.Integrate(f)

}

// GetStorageCondition 获取一个服务器的状态
func (c *Client) GetStorageCondition(addr string) (int, int, int, int, []app.LoadDate, error) {
	host, err := clientTorch.PathHostsToAddr(addr)
	if err != nil {
		return 0, 0, 0, 0, nil, err
	}

	return api.GetStorageCondition(host)

}

//GetTrackerVole 获取一个tracker的role
func (c *Client) GetTrackerVole(host *etc.Addr) (string, error) {
	return api.GetTrackerVole(host)
}

//GetLoadHosts 获取全部的集群节点
func (c *Client) GetLoadHosts() (traHosts []*etc.Addr, stoHosts []*etc.Addr, err error) {
	host := c.getLeader()

	return api.GetLoadHosts(host)
}

// UploadSmallFileByFile 上传小文件接口，通过文件
func (c *Client) UploadSmallFileByFile(filePath string) (string, error) {

	f, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer f.Close()

	hash, err := hashTorch.Integrate(f)
	if err != nil {
		return "", err
	}

	//只处理小文件
	if hash[len(hash)-1] != '0' {
		return "", storage.SmallFileSizeErr
	}

	ext := path.Ext(filePath)

	if c.VerifyFileIsExistByHash(hash, ext) {
		fk := fileKeyTorch.MakeFileKeyByHash(hash, ext)
		if fk != nil {
			fmt.Println("awd")
			return fk.GeyToken(), nil
		}
	}

	traHost, err := c.GetStorageAddr()
	if err != nil {
		return "", err
	}

	return api.UploadSmallFileByFile(traHost, f, hash, ext)

}

//获取一个EsoOK的下载地址
func (c *Client) getEsoOkHost(token string) (*etc.Addr, error) {
	eso, err := c.GetEso(token)

	var host *etc.Addr //请求用
	if err == nil {
		switch eso.Status {
		case app.Transmitting:
			//文件传输中，无法下载
			return nil, api.FileTransferErr
		case app.Synchronous:
			//文件同步中，转到保存中
			addr, err := clientTorch.PathHostsToAddr(eso.Hosts)
			if err != nil {
				return nil, err
			}

			host = addr

		case app.Ok:
			// 文件已经同步，使用负载均衡
			addr, err := c.GetStorageAddr()
			if err != nil {
				return nil, err
			}

			host = addr

		}

	} else {
		return nil, api.FileExitsErr
	}

	return host, nil
}

//GetSmallFileByFile 下载小文件到文件
func (c *Client) GetSmallFileByFile(token string, savePath string) error {

	host, err := c.getEsoOkHost(token)
	if err != nil {
		return err
	}

	return api.GetSmallFileByFile(host, token, savePath)

}

// GetSmallFileByByte 下载文件到Byte
func (c *Client) GetSmallFileByByte(token string) ([]byte, error) {
	eso, err := c.GetEso(token)

	var host *etc.Addr //请求用
	if err == nil {
		switch eso.Status {
		case app.Transmitting:
			//文件传输中，无法下载
			return nil, api.FileTransferErr
		case app.Synchronous:
			//文件同步中，转到保存中
			addr, err := clientTorch.PathHostsToAddr(eso.Hosts)
			if err != nil {
				return nil, err
			}

			host = addr

		case app.Ok:
			// 文件已经同步，使用负载均衡
			addr, err := c.GetStorageAddr()
			if err != nil {
				return nil, err
			}

			host = addr

		}

	} else {
		return nil, api.FileExitsErr
	}

	return api.GetSmallFileByByte(host, token)

}

//UploadMaxFileByPath 上传大文件通过Path
func (c *Client) UploadMaxFileByPath(filePath string) (string, error) {

	hash, err := c.GetHashByFile(filePath)
	if err != nil {
		return "", err
	}

	f, _ := os.Open(filePath)
	defer f.Close()

	ext := path.Ext(filePath)

	buf := make([]byte, int(etc.SysConf.StorageConf.UnitSize)*1024)

	fk, err := MakeFileTransferManagerByHash(hash, ext)
	if err != nil {
		return "", err
	}

	for true {
		n, err := f.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			}

			return "", err
		}

		err = fk.Upload(buf[:n])
		if err != nil {
			return "", err
		}
	}

	token, err := fk.UploadEnd()
	if err != nil {
		return "", err
	}

	return token, nil

}

// GetMaxFileByPath 下载大文件到本地，单线程
func (c *Client) GetMaxFileByPath(token string, filePath string) error {

	fk, err := MakeFileTransferManagerByToken(token)
	if err != nil {
		return err
	}

	fs, err := os.Create(filePath)
	if err != nil {
		return err
	}

	defer fs.Close()

	for true {
		bytes, err := fk.DownLoad()
		if err != nil {
			if err == io.EOF {
				return nil
			}
			return err
		}

		fs.Write(bytes)
	}

	return err

}

// GetMaxFileByPathHeight 下载大文件到本地，多线程下载
func (c *Client) GetMaxFileByPathHeight(token string, filePath string) error {

	host, err := c.getEsoOkHost(token)
	if err != nil {
		return err
	}
	flag := 0

	f, err := os.Create(filePath)
	if err != nil {
		return err
	}

	defer f.Close()

	protoList := make([]downProto, proto)

	//开始下载
	for i := 0; i < proto; i++ {
		protoList[i].start(int64(i), token, host)
	}

	for true {

		if flag == proto {
			return err
		}
		for i := 0; i < proto; i++ {
			buf, ok := protoList[i].getBuf()
			if !ok { //管道关闭判断
				if !protoList[i]._close {
					protoList[i]._close = true
					flag += 1
				}
			}

			f.Write(buf)

		}

	}

	return nil

}

// GetTrackerLog 获取tracker日志
func (c *Client) GetTrackerLog(host string, startTime string, endTime string, level string) ([]string, error) {
	addr, err := clientTorch.PathHostsToAddr(host)
	if err != nil {
		return nil, err
	}

	return api.GetTrackerLog(addr, startTime, endTime, level)

}

// GetStorageLog 获取Storage的日志
func (c *Client) GetStorageLog(host string, startTime string, endTime string, level string) ([]string, error) {
	addr, err := clientTorch.PathHostsToAddr(host)
	if err != nil {
		return nil, err
	}

	return api.GetStorageLog(addr, startTime, endTime, level)

}
