import React from 'react';
import {Route} from "react-router-dom"
import {Content} from "antd/es/layout/layout";

import Upload from './upload';
import Download from "./download";


function Test() {

    return (


        <Content
            className="site-layout-background"
            style={{
                padding: 0,
                margin: 0,
                minHeight: 100,
            }}
        >

            <Route component={Upload}/>
            <Route component={Download}/>


        </Content>

    )

}

export default Test;