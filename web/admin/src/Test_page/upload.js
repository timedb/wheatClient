import {Button, DatePicker, message, PageHeader, Upload,Form} from "antd";
import {InboxOutlined,UploadOutlined} from "@ant-design/icons";
import './css/upload.css'
import Axios from "axios";
import ServerApi from "../api/const";



const {Dragger} = Upload;
const {RangePicker} = DatePicker;
const normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
        return e;
    }
    return e && e.fileList;
};
function Upload2(prop) {

    const UploadFileApi= ServerApi.TestFileUpload
    return (
        <div className={"content1"}>
            <Form.Item >
                <Form.Item name="dragger" valuePropName="fileList" getValueFromEvent={normFile} noStyle>
                    <Upload.Dragger name="f1" action={UploadFileApi}>
                        <p className="ant-upload-drag-icon">
                            <InboxOutlined />
                        </p>
                        <p className="ant-upload-text">Click or drag file to this area to upload</p>
                        <p className="ant-upload-hint">Support for a single or bulk upload.</p>
                    </Upload.Dragger>
                </Form.Item>
            </Form.Item>
        </div>

    )
}
export default Upload2;