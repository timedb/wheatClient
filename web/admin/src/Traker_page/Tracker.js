import React from 'react';
import './css/Tracker.css'
import {Content} from "antd/es/layout/layout";
import {Route} from "react-router-dom";
import Colony from "./colony";
import File from "./file";
import Log from "./log"

function Tracker(props) {


    return (
        <Content
            // className="site-layout-background"
            // style={{
            //     padding: 0,
            //     margin: 0,
            //     // minHeight: 1000,
            // }}
        >

            <Route component={Colony}/>

            <Route component={File}/>

            <Route component={Log}/>

        </Content>

    );

}

export default Tracker;