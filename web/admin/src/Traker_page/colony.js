import {PageHeader, Table} from "antd";
import React, {useEffect, useRef, useState} from "react";
import Axios from "axios";
import ServerApi from "../api/const";
import './css/colony.css'

const {Column, ColumnGroup} = Table;

const options = [
    {
        value: 'ERROR',
        label: 'ERROR',

    },
    {
        value: 'DEBUG',
        label: 'DEBUG',

    },
    {
        value: 'WARNING',
        label: 'WARNING',

    },
    {
        value: 'CRITICAL',
        label: 'CRITICAL',

    },
];
//tracker集群表测试数据

//上传文件测试数据
const columns2 = [
    {
        title: 'TITLE',
        dataIndex: 'title',
        width: 200,
    },
    {
        title: 'SIZE',
        dataIndex: 'size',
        width: 150,
    },
    {
        title: 'HASH',
        dataIndex: 'hash',
        ellipsis: {
            showTitle: false,
        },
    },
];

//log测试数据
const columns3 = [
    {
        title: 'DATE',
        dataIndex: 'date',
        width: 150,
    },
    {
        title: 'TYPE',
        dataIndex: 'type',
        width: 150,
    },
    {
        title: 'MESSAGE',
        dataIndex: 'message',
        ellipsis: {
            showTitle: false,
        },
    },
];


function Colony(ComposedComponent) {

    const [TrackerCluster, setTrackerCluster] = useState()

    //读取Tracker集群信息
    const getTrackerCluster = function () {
        Axios(
            {
                url: ServerApi.TrackerClusterApi,
                method: "get",
            }
        ).then(function (response) {
            if (response.status === 200) {
                setTrackerCluster(response.data.data)
            }
        }).catch()
    }

    const timerID = useRef();
    useEffect(() => {
        getTrackerCluster();
    }, []);



    // timerID.current = setInterval(() => {
    //     getTrackerCluster();
    // }, 5000);




    return (
        <div className={"content1"}>
            <PageHeader
                className="site-page-header"
                // onBack={() => null}
                title="Tracker 集群"
                // subTitle="This is xxxxx"
            />
            <Table dataSource={TrackerCluster}>

                <Column title="IP" dataIndex="ip" key="ip"/>
                <Column title="PONT" dataIndex="pont" key="pont"/>

                <Column title="VOTE" dataIndex="vote" key="vote"/>
                <Column title="STATE" dataIndex="state" key="state"/>


            </Table>
        </div>
    )
}



export default Colony;