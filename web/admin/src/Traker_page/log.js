import {Cascader, ConfigProvider, DatePicker, PageHeader, Table} from "antd";
import React, {useEffect, useRef, useState} from "react";
import Axios from "axios";
import ServerApi from "../api/const";
import locale from "antd/lib/locale/zh_CN";
import './css/log.css'


const {Column, ColumnGroup} = Table;

const {RangePicker} = DatePicker;

const options = [
    {
        value: 'ERROR',
        label: 'ERROR',

    },
    {
        value: 'DEBUG',
        label: 'DEBUG',

    },
    {
        value: 'WARNING',
        label: 'WARNING',

    },
    {
        value: 'CRITICAL',
        label: 'CRITICAL',

    },
];
//tracker集群表测试数据

//上传文件测试数据
const columns2 = [
    {
        title: 'TITLE',
        dataIndex: 'title',
        width: 200,
    },
    {
        title: 'SIZE',
        dataIndex: 'size',
        width: 150,
    },
    {
        title: 'HASH',
        dataIndex: 'hash',
        ellipsis: {
            showTitle: false,
        },
    },
];

//log测试数据
const columns3 = [
    {
        title: 'DATE',
        dataIndex: 'date',
        width: 150,
    },
    {
        title: 'TYPE',
        dataIndex: 'type',
        width: 150,
    },
    {
        title: 'MESSAGE',
        dataIndex: 'message',
        ellipsis: {
            showTitle: false,
        },
    },
];

function Log(props) {

    const [TrackerLogFile, setTrackerLogFile] = useState()
    const startTime = useRef();
    const endTime = useRef();
    const logType = useRef();
    const time = useRef();
    const TimeChange = (dates, dateStrings) => {
        startTime.current = dateStrings[0];
        endTime.current = dateStrings[1];
        time.current = dates;
        getTrackerLogFile()
    }
    const logTypeChange = (value) => {
        logType.current = value;
        getTrackerLogFile()
    }

    //读取Tracker的Log文件
    const getTrackerLogFile = function () {

        Axios(
            {
                url: ServerApi.TrackerLogFileApi,
                method: "get",
                params: {
                    "startTime": startTime.current,
                    "endTime": endTime.current,
                    "logType": logType.current
                }
            }
        ).then(function (response) {
            if (response.status === 200) {
                setTrackerLogFile(response.data.data)
            }
        }).catch()
    }
    const timerID = useRef();
    useEffect(() => {
        getTrackerLogFile();
        timerID.current = setInterval(() => {
            getTrackerLogFile();
        }, 5000);
    }, []);
    //初始化HttpApi.md

    return (
        <div className={"content3"}>
            <PageHeader
                className="site-page-header"
                // onBack={() => null}
                title="LOG 日志"
            />
            <div className={"contentbox3-select"}>
                <span>选择日期：</span>
                <ConfigProvider locale={locale}>
                    <RangePicker showTime={{format: 'HH:mm'}} format="YYYY-MM-DD HH:mm"
                                 onChange={TimeChange}
                                 value={time.current}
                    />

                </ConfigProvider>
                <span>选择日志类型：</span>
                <Cascader className={"contentbox3-select-type"} options={options} onChange={logTypeChange}
                          placeholder="Type"/>
            </div>

            <Table columns={columns3} dataSource={TrackerLogFile} pagination={{pageSize: 5}} scroll={{y: 240}}/>

        </div>
    )
}

export default Log;