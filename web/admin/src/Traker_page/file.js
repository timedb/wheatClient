import {DatePicker, PageHeader, Table} from "antd";
import React, {useEffect, useRef, useState} from "react";
import Axios from "axios";
import ServerApi from "../api/const";
import './css/file.css'

const options = [
    {
        value: 'ERROR',
        label: 'ERROR',

    },
    {
        value: 'DEBUG',
        label: 'DEBUG',

    },
    {
        value: 'WARNING',
        label: 'WARNING',

    },
    {
        value: 'CRITICAL',
        label: 'CRITICAL',

    },
];
//tracker集群表测试数据

//上传文件测试数据
const columns2 = [
    {
        title: 'TITLE',
        dataIndex: 'title',
        width: 200,
    },
    {
        title: 'SIZE',
        dataIndex: 'size',
        width: 150,
    },
    {
        title: 'HASH',
        dataIndex: 'hash',
        ellipsis: {
            showTitle: false,
        },
    },
];

//log测试数据
const columns3 = [
    {
        title: 'DATE',
        dataIndex: 'date',
        width: 150,
    },
    {
        title: 'TYPE',
        dataIndex: 'type',
        width: 150,
    },
    {
        title: 'MESSAGE',
        dataIndex: 'message',
        ellipsis: {
            showTitle: false,
        },
    },
];


function File(props) {
    const [RecentlyFile, setRecentlyFile] = useState()

    //读取最近上传文件
    const getRcentlyFile = function () {
        Axios(
            {
                url: ServerApi.RecentlyFileApi,
                method: "get",
            }
        ).then(function (response) {
            if (response.status === 200) {
                setRecentlyFile(response.data.data)
            }
        }).catch()
    }

    const timerID = useRef();
    useEffect(() => {
        getRcentlyFile();
        timerID.current = setInterval(() => {
            getRcentlyFile();
        }, 5000);
    }, []);
    //初始化HttpApi.md

    return (
        <div className={"content2"}>
            <PageHeader
                className="site-page-header"
                // onBack={() => null}
                title="最近上传的文件"
            />
            <Table columns={columns2} dataSource={RecentlyFile} pagination={{pageSize: 5}} scroll={{y: 240}}/>
        </div>
    )
}

export default File;