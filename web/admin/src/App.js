import './App.css';
import {Breadcrumb, Layout, Menu} from 'antd';
import {BrowserRouter as Router, Link, Route} from 'react-router-dom'
import Tracker from "./Traker_page/Tracker"
import storage from "./Storage_page/storage";
import Test from "./Test_page/Test";

const {SubMenu} = Menu;
const {Header, Content, Footer, Sider} = Layout;


function App() {
    return (
        <Router>
            <meta name="viewport" content="width=device-width,initial-scale=1"/>
            <div>
                <Layout>
                    <Header className="header">
                        <div className="logo"/>
                        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                            <Menu.Item key="1">WheatDFS后台管理</Menu.Item>
                        </Menu>
                    </Header>
                    <Layout>
                        <Sider width={200} className="site-layout-background">
                            <Menu
                                mode="inline"
                                defaultSelectedKeys={['1']}
                                defaultOpenKeys={['sub1']}
                                style={{height: '100%', borderRight: 0}}
                            >
                                <SubMenu key="sub1" title="Storage" de>
                                    <Menu.Item key="1">
                                        <Link to={{pathname:"/storage", state: {ip: "192.168.31.120:8080"}}}>storage1</Link>

                                    </Menu.Item>
                                </SubMenu>
                                <Menu.Item><Link to={{pathname:"/Tracker",state: {ip: "192.168.31.120:8080"}}}>Tracker</Link></Menu.Item>

                                <Menu.Item key="sub3">
                                    <Link to={{pathname:"/Test",state: {ip: "192.168.31.120:8080"}}}>Test</Link>
                                </Menu.Item>

                            </Menu>
                        </Sider>
                        <Layout style={{padding: '0 24px 24px'}}>
                            <Breadcrumb style={{margin: '16px 0'}}>
                            </Breadcrumb>
                            <Content
                                className="site-layout-background"
                                style={{
                                    padding: 0,
                                    margin: 0,
                                    // minHeight: 1000,
                                }}
                            >
                                <Route path={"/storage"} component={storage}/>
                                <Route path={"/Tracker"} component={Tracker}/>
                                <Route path={"/Test"} component={Test}/>


                            </Content>
                        </Layout>
                    </Layout>
                </Layout>
                <Footer style={{textAlign: 'center'}}>web由react和antdesign开发</Footer>
            </div>
        </Router>
    )
}

export default App;
