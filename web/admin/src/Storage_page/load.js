import {Col, DatePicker, PageHeader, Row, Statistic, Table} from "antd";
import {Chart, Line, Point, Tooltip} from "bizcharts";
import React, {useEffect, useRef, useState} from "react";
import './css/load.css'
import Axios from "axios";
import ServerApi from "../api/const";
import './css/storage.css'

// contentbox2数据源 测试
const data = [
    {
        year: "1991",
        value: 10,
    },
    {
        year: "1992",
        value: 4,
    },
    {
        year: "1993",
        value: 3.5,
    },
    {
        year: "1994",
        value: 5,
    },
    {
        year: "1995",
        value: 4.9,
    },
    {
        year: "1996",
        value: 6,
    },
    {
        year: "1997",
        value: 7,
    },
    {
        year: "1998",
        value: 9,
    },
    {
        year: "1999",
        value: 13,
    },
];

function Load(props) {
    const [picture,setPicture] = useState()
    const [uploadNum, setUploadNum] = useState()
    const [sync, setSync] = useState()
    const [downNum, setDownNum] = useState()
    const [load, setLoad] = useState()


    const GetStorageLoadData = () => {
        Axios(
            {
                url: ServerApi.StorageLoadData,
                method: "get",
                params: {
                    "storage_ip": props.location.state.ip,
                }
            }
        ).then(function (response) {
            setPicture(response.data.data.picture)
            setUploadNum(response.data.data.uploadNum)
            setDownNum(response.data.data.downNum)
            setSync(response.data.data.sync)
            setLoad(response.data.data.load)
        }).catch()

    }



    const timerID = useRef();


    useEffect(() => {

        GetStorageLoadData();
        timerID.current = setInterval(() => {

            GetStorageLoadData();
        }, 5000);

    }, []);

    return (
        <div className={"contentbox2"}>
            <PageHeader
                className="site-page-header"
                // onBack={() => null}
                title="负载均衡"

            />
            <Chart
                appendPadding={[10, 0, 0, 10]}
                autoFit
                height={250}
                width={500}
                data={picture}
                scale={{value: {min: 0, alias: '人均年收入', type: 'linear-strict'}, year: {range: [0, 1]}}}
            >

                <Line position="year*value"/>
                <Point position="year*value"/>
                <Tooltip showCrosshairs/>
            </Chart>
            <div className={"contentbox2_value"}>
                <Row gutter={80}>
                    <Col span={10}>
                        <Statistic title="上传数" value={uploadNum}/>
                        <br/>
                        <Statistic title="下载数" value={downNum}/>

                    </Col>
                    <Col span={10}>
                        <Statistic title="同步" value={sync}/>
                        <br/>
                        <Statistic title="负载" value={load}/>

                    </Col>

                </Row>

            </div>

        </div>
    )
}

export default Load;