import './css/storage.css'
import React from 'react';
import {Content} from "antd/es/layout/layout";
import States from './state'
import {Route} from "react-router-dom";
import Load from './load'
import Log from './log'


function Storage(props) {
    // alert(props.location.state.ip)
    return (
        <Content
            // className="site-layout-background"
            // style={{
            //     padding: 0,
            //     margin: 0,
            //     // minHeight: 800,
            // }}
        >

            {/*<meta name="viewport" content="width=device-width,initial-scale=1"/>*/}
            <Route component={States}/>
            <Route component={Load}/>
            <Route component={Log}/>
        </Content>
    )

}

export default Storage;