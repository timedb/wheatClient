import {DatePicker, PageHeader, Progress, Table} from "antd";
import './css/state.css'
import ServerApi from "../api/const";
import React, {useEffect, useRef, useState} from 'react';
import Axios from "axios";
import './css/storage.css'
const {Column, ColumnGroup} = Table;

function State(props) {

    const [cpu, setCpu] = useState()
    const [memory, setMemory] = useState()
    const [disk, setDisk] = useState()
    const GetStorageState = () => {
        Axios(
            {
                url: ServerApi.StorageState,
                method: "get"
            }
        ).then(function (response) {
            setCpu(response.data.data.cpu)
            setDisk(response.data.data.disk)
            setMemory(response.data.data.memory)
        }).catch()


    }

    const timerID = useRef();


    useEffect(() => {
        GetStorageState();
        timerID.current = setInterval(() => {
            GetStorageState();
        }, 5000);

    }, []);
    return (
        <div className={"contentbox1"}>
            <PageHeader
                className="site-page-header"
                // onBack={() => null}
                title="Storage 状态"
                // subTitle="This is xxxxx"
            />
            <Progress className={"box1-1"} type="circle" percent={memory}/>
            <Progress className={"box1-2"} type="circle" percent={cpu}/>
            <Progress className={"box1-3"} type="circle" percent={disk}/>
            <span>负载 次/秒</span>
            <div className={"contentbox1-text"}>
                <span>内存使用率</span>
                <span>CPU使用率</span>
                <span>磁盘状态</span>
            </div>
        </div>
    )
}

export default State;