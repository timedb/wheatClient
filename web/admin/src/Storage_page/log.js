import {Cascader, ConfigProvider, DatePicker, PageHeader, Table} from "antd";
import locale from "antd/lib/locale/zh_CN";
import React, {useEffect, useRef, useState} from "react";
import Axios from "axios";
import ServerApi from "../api/const";
import './css/log.css'
const {Column, ColumnGroup} = Table;
const {RangePicker} = DatePicker;

// contentbox2数据源 测试
const options = [
    {
        value: 'ERROR',
        label: 'ERROR',

    },
    {
        value: 'DEBUG',
        label: 'DEBUG',

    },
    {
        value: 'WARNING',
        label: 'WARNING',

    },
    {
        value: 'CRITICAL',
        label: 'CRITICAL',

    },
];
//表格测试数据
const columns = [
    {
        title: 'DATE',
        dataIndex: 'date',
        width: 150,
    },
    {
        title: 'TYPE',
        dataIndex: 'type',
        width: 150,
    },
    {
        title: 'MESSAGE',
        dataIndex: 'message',
        ellipsis: {
            showTitle: false,
        },
    },
];
function Log(props) {

    const [log, setLog] = useState()
    const time = useRef();

    const startTime = useRef();
    const endTime = useRef();
    const logType = useRef();
    const TimeChange = (dates, dateStrings) => {
        startTime.current = dateStrings[0];
        endTime.current = dateStrings[1];
        time.current = dates;
        GetStorageLog();
    }
    const logTypeChange = (value) => {
        logType.current = value
        GetStorageLog();
    }
    const GetStorageLog = () => {
        Axios(
            {
                url: ServerApi.StorageLog,
                method: "get",
                params: {
                    "startTime": startTime.current,
                    "endTime": endTime.current,
                    "logType": logType.current
                }
            }
        ).then(function (response) {
            if (response.status === 200) {
                setLog(response.data.data)
            }
        }).catch()
    }

    const timerID = useRef();


    useEffect(() => {
        GetStorageLog();
        timerID.current = setInterval(() => {
            GetStorageLog();
        }, 5000);

    }, []);

    return (
        <div className={"contentbox3"}>
            <PageHeader
                className="site-page-header"
                // onBack={() => null}
                title="LOG 日志"
            />
            <div className={"contentbox3-select"}>
                <span>选择日期：</span>
                <ConfigProvider locale={locale}>
                    <RangePicker showTime={{format: 'HH:mm'}} format="YYYY-MM-DD HH:mm"
                                 onChange={TimeChange}
                                 value={time.current}
                    />

                </ConfigProvider>
                <span>选择日志类型：</span>
                <Cascader className={"contentbox3-select-type"} options={options} onChange={logTypeChange}
                          placeholder="Type"/>
            </div>

            <br/>
            <Table columns={columns} dataSource={log} pagination={{pageSize: 5}} scroll={{y: 240}}/>
        </div>
    )
}

export default Log;