package api

import (
	"errors"
	"github.com/timedb/wheatDFS/app"
	"github.com/timedb/wheatDFS/etc"
	"github.com/timedb/wheatDFS/fileKeyTorch"
	"io"
	"io/ioutil"
	"os"
)

// trackerApi

// GetStoAddr 获取storage负载地址
func GetStoAddr(host *etc.Addr) (*etc.Addr, error) {
	req := app.MakeTraGetStoAddr()
	resp := new(app.TraGetStoAddrResp)

	err := req.Do(host, resp)
	if err != nil {
		return nil, err
	}

	if !resp.Successful() {
		return nil, errors.New(resp.Err)
	}

	return resp.RespHost, nil

}

// GetLoadHosts 获取全部的服务器地址
func GetLoadHosts(host *etc.Addr) (traHosts []*etc.Addr, stoHosts []*etc.Addr, err error) {
	req := app.MakeTraReportTraHosts(nil, nil)
	resp := new(app.TraReportTraHostsResp)

	err = req.Do(host, resp)
	if err != nil {
		return nil, nil, err
	}

	if !resp.Successful() {
		return nil, nil, errors.New(resp.Err)
	}

	traHosts = resp.TraHosts
	stoHosts = resp.StoHosts

	err = nil
	return
}

// VerifyFileIsExist 判断文件是否存在
func VerifyFileIsExist(host *etc.Addr, hash string, ext string) bool {

	fk := fileKeyTorch.MakeFileKeyByHash(hash, ext)
	if fk == nil {
		return false
	}
	token := fk.GeyToken()

	req := app.MakeTraGetEsoDataReq(token)
	resp := new(app.TraGetEsoDataResp)

	err := req.Do(host, resp)
	if err != nil {
		return false
	}

	if !resp.Successful() {
		return false
	}

	return true

}

func GetExo(host *etc.Addr, token string) (*app.EsoData, error) {
	req := app.MakeTraGetEsoDataReq(token)
	resp := new(app.TraGetEsoDataResp)

	err := req.Do(host, resp)
	if err != nil {
		return nil, err
	}

	if !resp.Successful() {
		return nil, errors.New(resp.Err)
	}

	return resp.Eso, nil

}

//UploadSmallFileByFile 根据path上传小文件
func UploadSmallFileByFile(host *etc.Addr, f io.Reader, hash string, ext string) (string, error) {

	buf, err := ioutil.ReadAll(f)
	if err != nil {
		return "", err
	}

	req := app.MakeStoUploadSmallFileReq(buf, hash, ext)
	resp := new(app.StoUploadSmallFileResp)

	err = req.Do(host, resp)

	if err != nil {
		return "", err
	}

	if !resp.Successful() {
		return "", errors.New(resp.Err)
	}

	return resp.FileKey, nil

}

// GetSmallFileByFile 下载大文件
func GetSmallFileByFile(host *etc.Addr, token string, savePath string) error {
	req := app.MakeStoGetSmallFileReq(token)
	resp := new(app.StoGetSmallFileResp)

	err := req.Do(host, resp)
	if err != nil {
		return err
	}

	if !resp.Successful() {
		return errors.New(resp.Err)
	}

	f, err := os.Create(savePath)
	if err != nil {
		return err
	}

	defer f.Close()

	f.Write(resp.Content)
	return nil

}

// GetSmallFileByByte 下载文件到byte中
func GetSmallFileByByte(host *etc.Addr, token string) ([]byte, error) {
	req := app.MakeStoGetSmallFileReq(token)
	resp := new(app.StoGetSmallFileResp)

	err := req.Do(host, resp)
	if err != nil {
		return nil, err
	}

	if !resp.Successful() {
		return nil, errors.New(resp.Err)
	}

	return resp.Content, nil
}

// GetStorageCondition 获取一个storage的服务信息
func GetStorageCondition(host *etc.Addr) (syncNum int, uploadNum int, downloadNum int, loadNum int, history []app.LoadDate, err error) {
	req := app.MakeStoGetConditionReq()
	resp := new(app.StoGetConditionResp)

	err = req.Do(host, resp)

	if err != nil {
		return
	}

	if !resp.Successful() {
		err = errors.New(resp.Err)
		return
	}

	syncNum = resp.Sync
	uploadNum = resp.Upload
	downloadNum = resp.Download
	loadNum = resp.Load
	history = resp.HistoryLoad
	err = nil
	return

}

//GetTrackerVole 获取一个tracker的角色
func GetTrackerVole(host *etc.Addr) (string, error) {
	req := app.MakeTraGetRoleReq()
	resp := new(app.TraGetRoleResp)

	err := req.Do(host, resp)

	if err != nil {
		return "", err
	}

	if !resp.Successful() {
		return "", errors.New(resp.Err)
	}

	return resp.RoleName, nil

}

// GetTrackerLog 获取tracker日志
func GetTrackerLog(host *etc.Addr, startTime string, endTime string, level string) ([]string, error) {
	req := app.MakeTraGetReq(startTime, endTime, level)
	resp := new(app.GetLogResp)

	//请求
	err := req.Do(host, resp)
	if err != nil {
		return nil, err
	}

	if !resp.Successful() {
		return nil, err
	}

	return resp.LogMsg, nil

}

// GetStorageLog 获取tracker日志
func GetStorageLog(host *etc.Addr, startTime string, endTime string, level string) ([]string, error) {
	req := app.MakeStoGetReq(startTime, endTime, level)
	resp := new(app.GetLogResp)

	//请求
	err := req.Do(host, resp)
	if err != nil {
		return nil, err
	}

	if !resp.Successful() {
		return nil, err
	}

	return resp.LogMsg, nil

}
