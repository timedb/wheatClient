package api

import (
	"errors"
)

var (
	FileTransferErr = errors.New("unable to download in file transfer")
	FileExitsErr    = errors.New("file does not exist")
)
