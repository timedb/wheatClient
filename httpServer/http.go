package httpServer

import (
	"gitee.com/timedb/wheatClient"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/timedb/wheatDFS/etc"
	serverTorch2 "github.com/timedb/wheatDFS/serverTorch"
	"log"
	"sync"
	"time"
)

var (
	sysServer *HttpServer
	lock      sync.Once
	sysClient *wheatClient.Client
)

type HttpServer struct {
	s         *gin.Engine //路由服务
	localHost *etc.Addr   //地址
}

// Start 启动服务
func (h *HttpServer) Start() {
	h.s.Run(h.localHost.GetAddress())
}

// MakeHttpServer 创建server
func MakeHttpServer() *HttpServer {
	lock.Do(func() {
		s := new(HttpServer)
		s.s = gin.Default()

		ip, err := serverTorch2.GetIPv4s()
		if err != nil {
			log.Fatalln(err)
		}

		//获取port
		port := etc.SysConf.Client.Port
		if port == "" {
			port, err = serverTorch2.GetAvailablePort()
			if err != nil {
				log.Fatalln(err)
			}
		}

		addr, err := etc.MakeAddr(ip, port, etc.StateDefault)
		if err != nil {
			log.Fatalln(err)
		}
		s.localHost = addr

		sysServer = s
	})

	return sysServer

}

// BindRouter  绑定路由和函数
func (h *HttpServer) BindRouter() {

	mwCORS := cors.New(cors.Config{
		//准许跨域请求网站,多个使用,分开,限制使用*
		AllowOrigins: []string{"*"},
		//准许使用的请求方式
		AllowMethods: []string{"*"},
		//准许使用的请求表头
		AllowHeaders: []string{"*"},
		//显示的请求表头
		ExposeHeaders: []string{"Content-Type"},
		//凭证共享,确定共享
		AllowCredentials: true,
		//容许跨域的原点网站,可以直接return true就万事大吉了
		AllowOriginFunc: func(origin string) bool {
			return true
		},
		//超时时间设定
		MaxAge: 24 * time.Hour,
	})

	//确定中间件，开启Cors
	h.s.Use(mwCORS)

	h.s.GET(GetTrackerRecentlyFile, GetTrackerRecentlyFileApi) //测试用api
	h.s.GET(GetTrackerCluster, GetTrackerClusterApi)
	h.s.GET(GetTrackerLog, GetTrackerLogApi)
	h.s.GET(GetStorageState, GetStorageStateApi)
	h.s.GET(GetStorageLog, GetStorageLogApi)
	h.s.GET(GetStorageLoadData, GetStorageLoadDataApi)
	h.s.GET(GetTestLog, GetTestLogApi)
	h.s.POST(GetTestDownFile, GetTestDownFileApi)
	h.s.POST(GetTestFileUpload, GetTestFileUploadApi)
}
