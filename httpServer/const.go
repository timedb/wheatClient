package httpServer

// GetTest api
var (
	GetTrackerRecentlyFile = "/get/RecentlyFile"
	GetTrackerCluster      = "/get/TrackerCluster"
	GetTrackerLog          = "/get/TrackerLogFile"
	GetStorageLog          = "/get/StorageLog"
	GetStorageLoadData     = "/get/StorageLoadData"
	GetStorageState        = "/get/StorageState"
	GetTestLog             = "/get/TestLog"
	GetTestDownFile        = "/get/TestDownFile"
	GetTestFileUpload      = "/get/TestFileUpload"
)
