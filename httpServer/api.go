package httpServer

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

/*
json规范
{
	state: 0,1,
	data: {
		content;
	},
}
*/

//Tracker的最近上传文件
func GetTrackerRecentlyFileApi(context *gin.Context) {

	data := make([]gin.H, 0)

	data = append(data, gin.H{
		"key":   "1",
		"title": "《菜鸡的自我修养》.doc",
		"size":  "200kb",
		"hash":  "csanuvhasuhewaufasf1e13",
	})
	data = append(data, gin.H{
		"key":   "2",
		"title": "《菜鸡的自我修养2》.doc",
		"size":  "200kb",
		"hash":  "csanuvhasuhewauffaifjaewuisf1e13",
	})

	context.JSON(http.StatusOK, gin.H{
		"state": 1,
		"data":  data,
	})
}

//Tracker的集群信息
func GetTrackerClusterApi(context *gin.Context) {
	data := make([]gin.H, 0)
	data = append(data, gin.H{
		"key":   '1',
		"ip":    "210.26.13.32",
		"pont":  "***************",
		"vote":  "ufhafhwuahfuwahfueah",
		"state": "超好",
	})
	data = append(data, gin.H{
		"key":   '2',
		"ip":    "210.26.13.80",
		"pont":  "***************",
		"vote":  "ufhafhwuahfuwahfueah",
		"state": "良好",
	})

	context.JSON(http.StatusOK, gin.H{
		"state": 1,
		"data":  data,
	})
}

//Tracker的日志接口
func GetTrackerLogApi(context *gin.Context) {
	data := make([]gin.H, 0)
	data2 := make([]gin.H, 0)
	data = append(data, gin.H{
		"key":      "1",
		"date":     "2021-04-01 15:04",
		"type":     "ERROR",
		"message":  "this very good",
		"textWrap": "word-break",
	})
	data = append(data, gin.H{
		"key":      "2",
		"date":     "2021-04-01 15:04",
		"type":     "DEBUG",
		"message":  "this NOT very good",
		"textWrap": "word-break",
	})
	layout := "2006-01-02 15:04"
	startTime := context.Query("startTime")
	endTime := context.Query("endTime")
	logType := context.Query("logType[]")
	//fmt.Println(startTime,endTime)
	t1, _ := time.Parse(layout, startTime)
	t2, _ := time.Parse(layout, endTime)
	var checkTime string
	var checkLogType string
	for i := range data {

		checkTime = data[i]["date"].(string)
		checkLogType = data[i]["type"].(string)
		t3, _ := time.Parse(layout, checkTime)
		if logType == "" {
			if int(t3.Sub(t1).Hours()/24) >= 0 && int(t3.Sub(t2).Hours()/24) <= 0 {

				data2 = append(data2, data[i])

			}
		} else {
			if int(t3.Sub(t1).Hours()/24) >= 0 && int(t3.Sub(t2).Hours()/24) <= 0 && logType == checkLogType {

				data2 = append(data2, data[i])

			}
		}

	}
	context.JSON(http.StatusOK, gin.H{
		"state": "1",
		"data":  data2,
	})
}

//Storage的状态
func GetStorageStateApi(context *gin.Context) {
	data := gin.H{
		"cpu":    13,
		"memory": 50,
		"disk":   75,
	}
	context.JSON(http.StatusOK, gin.H{
		"state": '1',
		"data":  data,
	})
}

//Storage的日志接口
func GetStorageLogApi(context *gin.Context) {
	data := make([]gin.H, 0)
	data2 := make([]gin.H, 0)
	data = append(data, gin.H{
		"key":      "1",
		"date":     "2021-05-04 15:04",
		"type":     "ERROR",
		"message":  "这是一个Err",
		"textWrap": "word-break",
	})
	data = append(data, gin.H{
		"key":      "2",
		"date":     "2021-04-12 15:04",
		"type":     "DEBUG",
		"message":  "这是一个DEBUG",
		"textWrap": "word-break",
	})
	layout := "2006-01-02 15:04"
	startTime := context.Query("startTime")
	endTime := context.Query("endTime")
	logType := context.Query("logType[]")
	//fmt.Println(startTime,endTime)
	t1, _ := time.Parse(layout, startTime)
	t2, _ := time.Parse(layout, endTime)
	var checkTime string
	var checkLogType string
	for i := range data {

		checkTime = data[i]["date"].(string)
		checkLogType = data[i]["type"].(string)
		t3, _ := time.Parse(layout, checkTime)
		if logType == "" {
			if int(t3.Sub(t1).Hours()/24) >= 0 && int(t3.Sub(t2).Hours()/24) <= 0 {

				data2 = append(data2, data[i])

			}
		} else {
			if int(t3.Sub(t1).Hours()/24) >= 0 && int(t3.Sub(t2).Hours()/24) <= 0 && logType == checkLogType {

				data2 = append(data2, data[i])

			}
		}

	}
	//print(startTime + "\n" + endTime + "\n")
	context.JSON(http.StatusOK, gin.H{
		"state": "1",
		"data":  data2,
	})
}

//获取Storage的负载数据
func GetStorageLoadDataApi(context *gin.Context) {
	picture := make([]gin.H, 0)
	picture = append(picture, gin.H{
		"year":  "1991",
		"value": 10,
	})
	picture = append(picture, gin.H{
		"year":  "1992",
		"value": 4,
	})
	picture = append(picture, gin.H{
		"year":  "1993",
		"value": 6,
	})
	picture = append(picture, gin.H{
		"year":  "1994",
		"value": 11,
	})
	picture = append(picture, gin.H{
		"year":  "1995",
		"value": 16,
	})
	picture = append(picture, gin.H{
		"year":  "1996",
		"value": 7,
	})
	data := gin.H{
		"uploadNum": 100,
		"sync":      231,
		"downNum":   102,
		"load":      199,
		"picture":   picture,
	}
	context.JSON(http.StatusOK, gin.H{
		"state": '1',
		"data":  data,
	})
}

//获取Test的Log
func GetTestLogApi(context *gin.Context) {
	data := make([]gin.H, 0)
	data2 := make([]gin.H, 0)
	data = append(data, gin.H{
		"key":      "1",
		"date":     "2021-04-04 15:04",
		"type":     "ERROR",
		"message":  "this Test very good",
		"textWrap": "word-break",
	})
	data = append(data, gin.H{
		"key":      "2",
		"date":     "2021-04-18 15:04",
		"type":     "DEBUG",
		"message":  "this Test NOT very good",
		"textWrap": "word-break",
	})
	layout := "2006-01-02 15:04"
	startTime := context.Query("startTime")
	endTime := context.Query("endTime")
	logType := context.Query("logType[]")
	//fmt.Println(startTime,endTime)
	t1, _ := time.Parse(layout, startTime)
	t2, _ := time.Parse(layout, endTime)
	var checkTime string
	var checkLogType string
	for i := range data {

		checkTime = data[i]["date"].(string)
		checkLogType = data[i]["type"].(string)
		t3, _ := time.Parse(layout, checkTime)
		if logType == "" {
			if int(t3.Sub(t1).Hours()/24) >= 0 && int(t3.Sub(t2).Hours()/24) <= 0 {

				data2 = append(data2, data[i])

			}
		} else {
			if int(t3.Sub(t1).Hours()/24) >= 0 && int(t3.Sub(t2).Hours()/24) <= 0 && logType == checkLogType {

				data2 = append(data2, data[i])

			}
		}

	}
	//print(startTime + "\n" + endTime + "\n")
	context.JSON(http.StatusOK, gin.H{
		"state": "1",
		"data":  data2,
	})
}

//获取Test的下载文件
func GetTestDownFileApi(context *gin.Context) {

	context.Writer.Header().Add("Content-Disposition", fmt.Sprintf("attachment; filename=%s", "ab23.txt")) //fmt.Sprintf("attachment; filename=%s", filename)对下载的文件重命名
	context.Writer.Header().Add("Content-Type", "application/octet-stream")
	context.File("D:\\Go\\wheatClient\\clientApi.md")

}

//Test文件上传
func GetTestFileUploadApi(c *gin.Context) {
	//获取表单数据 参数为name值
	f, err := c.FormFile("f1")
	//错误处理
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	} else {
		//将文件保存至本项目根目录中
		dst := "D:/Go/wheatClient/uploadFile/"
		c.SaveUploadedFile(f, dst+f.Filename)
		//保存成功返回正确的Json数据
		c.JSON(http.StatusOK, gin.H{
			"message": "OK",
		})
	}
}
