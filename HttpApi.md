### 1. 测试用接口

- 接口名

| Url                 | R    |
| ------------------- | ---- |
| /get/RecentlyFile   | Get  |
| /get/TrackerCluster | Get  |
| /get/TrackerLogFile | Get  |

- 请求参数

无

- 响应

```json
{
  "data":{
		"key":   "1",
		"title": "《菜鸡的自我修养》.doc",
		"size":  "200kb",
		"hash":  "csanuvhasuhewaufasf1e13",
	},
  "state": 1
}
```

```json
{
  "data":{
		"key":   '1',
		"ip":    "210.26.13.32",
		"pont":  "***************",
		"vote":  "ufhafhwuahfuwahfueah",
		"state": "超好",
	}),
  "state": 1
}
```

```json
{
  "data":{
		"key":      "1",
		"date":     "2021-04-01",
		"type":     "ERROR",
		"message":  "this very good",
		"textWrap": "word-break",
	}),
  "state": 1
}
```

