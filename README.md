# wheatClient

### 介绍

goDfs的go语言版客户端

### 安装教程

使用gomod安装管理依赖

```shell
go get gitee.com/timedb/wheatClient
```

### 使用说明

该项目为[wheatDFS](https://gitee.com/timedb/goDFS)的go语言客户端，提供了，对接wheatDFS的各种API。

#### 上传

##### 上传文件

```go
func (c *Client) UploadFile(file multipart.File, name string) (string, error)  //上传文件的函数原型

//使用方法
client, err := wheatClient.NewClient()  //创建一个用于连接服务器的客户端
f, _ := os.Open("filePath")  //打开一个文件
defer f.Close()
key, err := client.UploadFile(f, "filePath.txt")  
if err != nil {
		fmt.Println(err)
		return
	}
fmt.Println(key)//group:group:path:d9/06:bcc249433b49f08718e2e4adbbdfc0cc.docx
```

**UploadFile**需要传入一个**multipart.File**接口，以及一个**string**，一个是文件本身，一个是文件名，该接口会向wheatDFS上传文件，并且访问一个用来访问文件的令牌。



##### 大文件上传的接口

```go
(f *FileTransferManager) Upload(buf []byte)//函数原型

//使用方法
fk, err := MakeFileTransferManagerByHash(hash, "mp4") //使用hash创建文件令牌
buf := make([]byte, int(etc.SysConf.StorageConf.UnitSize)*1024)
for {
	n, err := f.Read(buf)
	if err == io.EOF {
		break
	}
	err = fk.Upload(buf[:n]) //上传大文件
	if err != nil {
		fmt.Println(err)
		return
	}
}
```



##### 通过文件地址上传小文件

```go
(c *Client) UploadSmallFileByFile(filePath string) (string, error)
//例
client := MakeWheatClient("F:\\GO\\wheatClient\\wheatDFS.ini")
path := "F:\\GO\\wheatClient\\HttpApi.md"
token, _ := client.UploadSmallFileByFile(path) //返回文件的令牌
```



##### 通过地址上传大文件

```go
(c *Client) UploadMaxFileByPath(filePath string) (string, error)
//例
client := MakeWheatClient("F:\\GO\\wheatClient\\wheatDFS.ini")
path := "F:\\学习\\Java\\JavaWeb\\code&资料.rar"
token, _:= client.UploadSmallFileByFile(path)
fmt.Println(token)
```



##### 上传结束

```go
(f *FileTransferManager) UploadEnd() (string, error) //函数原型
//示例
token, err := fk.UploadEnd() //在上传文件结束时,调用此方法得到文件令牌
```





#### 下载

##### 验证文件是否存在

```go
func (c *Client) VerKey(key string) (bool, string) //函数原型

//使用方法
client, _ := wheatClient.NewClient()
b, addr := client.VerKey("group:group:path:d9/06:bcc249433b49f08718e2e4adbbdfc0cc.docx")//传入令牌
fmt.Println(b, addr)

```

**VerKey**可以判断一个文件令牌是否存在，返回一个bool和string，bool为true则文件存在，同时返回wheatDFS同步前的储存地址。



##### 下载文件

```go
func (c *Client) GetFile(key string) (*WheatFile, string, error) //函数原形

//使用方法
key := "max:group/04/c1/876b47715a8629addae954f8fa2db0f1.mp4"
client, _ := wheatClient.NewClient()
f, _, err := client.GetFile(key)

```

**GetFile**可以使用一个文件令牌来下载文件，这个接口会返回，一个WheatFile指针，以及string表示文件名，我们可以操作WheatFile来得到文件内容。



##### 单线程文件流下载

```go
(f *FileTransferManager) DownLoad() ([]byte, error) //函数原型
//例:
token := "group/67/48/d49a4b900d1186a6157a0b604d6158b41.mp4" //下载的文件令牌
fk, _ := MakeFileTransferManagerByToken(token) //创建下载令牌使用token
f, _ := os.Create("F:/视频/MyTv/wad.mp4")
defer f.Close()
for true {
	buf, err := fk.DownLoad()
	if err != nil {
		if err == io.EOF {
			break
		}
		fmt.Println(err)
		return
		}
		f.Write(buf)
}
```



##### 下载小文件到指定的文件

```go
(c *Client) GetSmallFileByFile(token string, savePath string) error
//例
client := MakeWheatClient("F:\\GO\\wheatClient\\wheatDFS.ini")
path := "F:\\test.txt"
token := "group/9b/96/3abf2ddabdae3ed0a26b91175d19c83a0.md"
client.GetSmallFileByFile(token, path)
```



##### 根据文件地址计算文件哈希

```go
GetHashByFile(filePath string) (string, error)
//例
client := MakeWheatClient("F:\\GO\\wheatClient\\wheatDFS.ini")
path := "F:\\GO\\wheatClient\\HttpApi.md"
hash, _ := client.GetHashByFile(path)
//这里hash就是指定文件的哈希
```



##### 切换下载模式

```go
(f *FileTransferManager) SwitchToDownloadMode() error//函数原型
//这个方法将上传模式转换成下载模式
//例
fk, _ := MakeFileTransferManagerByHash("filehash", "fileExt")
fk.SwitchToDownloadMode()
```





#### 获取服务器信息

##### 创建配连接器

```go
MakeWheatClient(conf string) *Client
//例
client := MakeWheatClient("D:\\goproject\\wheatClient\\wheatDFS.ini")
//其中传入的参数为ini所在的存储地址
```



##### 获取storage的信息

```go
(c *Client) GetStorageAddr() (*etc.Addr, error)//函数原型
//例
client := MakeWheatClient("F:\\GO\\wheatClient\\wheatDFS.ini")
addr, _ := client.GetStorageAddr()
//这里的addr就是storage的信息, 如果想获取多个,可以再次调用方法
//例
client := MakeWheatClient("F:\\GO\\wheatClient\\wheatDFS.ini")
Addr1, _ := client.GetStorageAddr()
Addr2, _ := client.GetStorageAddr()
```



##### 获取一个eso数据包

```go
(c *Client) GetEso(token string) (*app.EsoData, error)
//例
client := MakeWheatClient("F:\\GO\\wheatClient\\wheatDFS.ini")
token := "group/13/b4/d4666f354946502a4fb8a21d0fa816e90.ini"
Data, err := client.GetEso(token)//Data就是Eso数据包
```



##### 检验一个Eso包是否存在

```go
(c *Client) VerifyFileIsExistByHash(hash string, ext string) bool
//例
client := MakeWheatClient("F:\\GO\\wheatClient\\wheatDFS.ini")
hash := "13b47de8467a09a9f73d0dd8fd1afee3d4666f354946502a4fb8a21d0fa816e90"
Ext := "ini"
isTrue := client.VerifyFileIsExistByHash(hash, Ext)
```



##### 获取服务器状态

```go
(c *Client) GetStorageCondition(addr string) (int, int, int, int, []app.LoadDate, error)
//例
client := MakeWheatClient("F:\\GO\\wheatClient\\wheatDFS.ini")
addr := "192.168.31.203"
a1, a2, a3, a4, a5, _ := client.GetStorageCondition(addr)
//返回的东西分别是同步文件数量,上传数量,下载数量,总共服务器被访问的次数,历史记录, error
```



##### 获取一个tracker的角色

```go
(c *Client) GetTrackerVole(host *etc.Addr) (string, error)
//例
client := MakeWheatClient("F:\\GO\\wheatClient\\wheatDFS.ini")
tra, sto, _ := client.GetLoadHosts()
for _, host := range tra {
	Role, _ := client.GetTrackerVole(host)
	fmt.Println(Role)
}
```



##### 获取全部的集群节点

```go
(c *Client) GetLoadHosts() (traHosts []*etc.Addr, stoHosts []*etc.Addr, err error)
//例
client := MakeWheatClient("F:\\GO\\wheatClient\\wheatDFS.ini")
tra, sto, _ := client.GetLoadHosts() //获取全部的节点
for _, host := range tra {
	Role, _ := client.GetTrackerVole(host)
	fmt.Println(Role)
}
```





#### 日志

##### 获取tracekr的日志信息

```go
(c *Client) GetTrackerLog(host string, startTime string, endTime string, level string) ([]string, error)
//例
client := MakeWheatClient("D:\\goproject\\wheatClient\\wheatDFS.ini")
tra, sto, err := client.GetLoadHosts()
start := "2021-05-20 21:00"
end := "2021-05-20 21:40"
log, err := client.GetTrackerLog(addr.GetAddress(), start, end, "")
```



##### 获取storage的日志信息

```go
(c *Client) GetStorageLog(host string, startTime string, endTime string, level string) ([]string, error)
//例
client := MakeWheatClient("F:\\GO\\wheatClient\\wheatDFS.ini")
start := "2021-05-20 21:00"
end := "2021-05-20 21:40"
addr, _ := client.GetStorageAddr()
logdata, _ := client.GetStorageLog(addr.Host, start, end, "")
```





