package wheatClient

import (
	"fmt"
	"testing"
)

//小文件上传下载
func TestClient_GetStorageAddr(t *testing.T) {
	client := MakeWheatClient("D:\\goproject\\wheatClient\\wheatDFS.ini")
	//addr, err := client.GetStorageAddr()
	//fmt.Println(addr, err)
	//
	token, err := client.UploadSmallFileByFile("D:\\goproject\\wheatClient\\HttpApi.md")
	fmt.Println(token, err)
	//
	//hash, _ := client.GetHashByFile("D:\\goproject\\wheatClient\\HttpApi.md")
	//
	//b := client.VerifyFileIsExistByHash(hash, "md")
	//
	//fmt.Println(b)

	//token := "group/9b/96/3abf3ddabdae3ed0a26b91175d19c83a0.md"

	//exo, err := client.GetEso(token)
	//fmt.Println(exo, err)
	//
	//err = client.GetSmallFileByFile(token, "./op.md")
	//fmt.Println(err)

	buf, err := client.GetSmallFileByByte(token)
	fmt.Println(buf, err)

}

func TestClient_GetStorageCondition(t *testing.T) {

	client := MakeWheatClient("D:\\goproject\\wheatClient\\wheatDFS.ini")

	//addr := "100.84.123.122:5591"
	//fmt.Println(client.GetStorageCondition(addr))

	tra, sto, _ := client.GetLoadHosts()
	for _, host := range tra {
		vole, _ := client.GetTrackerVole(host)
		fmt.Println(vole)
	}

	for _, host := range sto {
		fmt.Println(client.GetStorageCondition(host.GetAddress()))
	}

	//token := "group/9b/96/3abf2ddabdae3ed0a26b91175d19c83a0.md"

}

func TestClient_GetTrackerLog(t *testing.T) {
	client := MakeWheatClient("D:\\goproject\\wheatClient\\wheatDFS.ini")

	tra, sto, err := client.GetLoadHosts()
	fmt.Println(tra, sto, err)

	start := "2021-05-20 21:00"
	end := ""

	for _, addr := range tra {
		log, err := client.GetTrackerLog(addr.GetAddress(), start, end, "ERROR")
		if err != nil {
			fmt.Println(err)
		}

		for _, ste := range log {
			fmt.Println(ste)
		}

	}

}

func TestFileTransferManager_Upload(t *testing.T) {
	client := MakeWheatClient("D:\\goproject\\wheatClient\\wheatDFS.ini")
	path := "D:\\ISO\\deepin-desktop-community-1002-amd64 (1).iso"

	token, err := client.UploadMaxFileByPath(path)
	fmt.Println(token, err)

}

func TestFileTransferManager_DownLoad(t *testing.T) {
	client := MakeWheatClient("D:\\goproject\\wheatClient\\wheatDFS.ini")
	//
	//token, err := client.UploadMaxFileByPath("F:\\视频\\MyTv\\fodfs\\使用.mp4")
	//
	//fmt.Println(token, err)

	token := "group/58/70/85529224e35b37ddc28dc13fe4d3b7181.iso"

	client.GetMaxFileByPathHeight(token, "F:\\视频\\MyTv\\abms.iso")

}

func TestClient_GetStorageLog(t *testing.T) {
	client := MakeWheatClient("D:\\goproject\\wheatClient\\wheatDFS.ini")
	tra, _, _ := client.GetLoadHosts()
	for _, val := range tra {

		logs, _ := client.GetTrackerLog(val.GetAddress(), "", "", "ERROR")
		for _, log := range logs {
			fmt.Println(log)
		}

	}

}
